<jsp:useBean id="weatherData" scope="request" type="com.eeducation.ztu.java_lab_14.weather.WeatherData"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Weather Information</title>
</head>
<body>
<h1>Weather Information</h1>
<c:if test="${not empty weatherData}">
    <p><b>Country:</b> ${weatherData.sys.country}</p>
    <p><b>City:</b> ${weatherData.name}</p>
    <p><b>Temperature:</b> ${weatherData.main.temp} K</p>
    <p><b>Wind Direction:</b> ${weatherData.wind.deg}</p>
    <p><b>Wind Speed:</b> ${weatherData.wind.speed} m/s</p>
</c:if>
</body>
</html>
