package com.eeducation.ztu.java_lab_14;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.io.PrintWriter;

@WebFilter("/secondServlet")
public class CoordFilter implements Filter {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String lat = request.getParameter("lat");
        String lon = request.getParameter("lon");
        if (lat != null && lon != null && !lat.isEmpty() && !lon.isEmpty()) {
            chain.doFilter(request, response);
        }
        else {
            PrintWriter out = response.getWriter();
            out.println("Невірні координати. Будь ласка, введіть вірні значення.");
        }
    }
}
