package com.eeducation.ztu.java_lab_14;

import com.eeducation.ztu.java_lab_14.weather.WeatherData;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

@WebServlet(name = "secondServlet", value = "/secondServlet")
public class SecondServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String latitude = request.getParameter("lat");
        String longitude = request.getParameter("lon");
        String apiKey = "77a6953da8e0ff2b9eab52d685224d64";

        String urlString = "https://api.openweathermap.org/data/2.5/weather?lat=" + latitude + "&lon=" + longitude
                + "&appid=" + apiKey;

        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder jsonResponse = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                jsonResponse.append(line);
            }
            reader.close();

            ObjectMapper objectMapper = new ObjectMapper();
            WeatherData weatherData = objectMapper.readValue(jsonResponse.toString(), WeatherData.class);

            request.setAttribute("weatherData", weatherData);
            request.getRequestDispatcher("weather.jsp").forward(request, response);
        } else {
            PrintWriter out = response.getWriter();
            out.println("Error: Failed to fetch weather data. Response code: " + responseCode);
        }
    }
    protected void doFilter(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    }

}
