<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>get</title>
</head>
<body>
<%if ("GET".equals(request.getMethod()))  {
    Cookie[] cookies = request.getCookies();
    String nameFromCookie = null;
    if (cookies != null) {
        for (Cookie cookie : cookies) {
            if ("name".equals(cookie.getName())) {
                nameFromCookie = cookie.getValue();
                break;
            }
        }
    }
%>
<h1>This is a GET request</h1>
<% if (nameFromCookie != null) { %>
<p>Hello, the name from cookie is: <%= nameFromCookie %></p>
<% } else { %>
<p>No name found in the cookie.</p>
<%
        }
    }
%>
</body>
</html>
